import * as knex from 'knex';
import Config from './config';

export const config = {
  client: 'pg',
  connection: Config.pg.url,
  migrations: {
    tableName: 'migrations',
  },
  pool: {
    min: 1,
    max: 7,
  },
};

let client = null;

export function connect() {
  if (!client) {
    client = knex(config);
  }

  return client;
}
