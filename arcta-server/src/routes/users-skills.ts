import * as userSkillService from '../services/user-skill-service';

export default (server) => {
  server.route({
    method: 'GET',
    path: '/users_skills',
    handler() {
      return userSkillService.getUserSkill();
    },
  });

  server.route({
    method: 'PUT',
    path: '/users_skills',
    handler(request) {
      return userSkillService.addUserSkill(request.payload);
    },
  });

  server.route({
    method: 'DELETE',
    path: '/users_skills',
    handler(request) {
      return userSkillService.delUserSkill(request.payload);
    },
  });
};
