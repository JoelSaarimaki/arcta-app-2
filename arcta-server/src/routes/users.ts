import * as userService from '../services/user-service';

export default (server) => {
  server.route({
    method: 'GET',
    path: '/users',
    handler() {
      return userService.getUsers();
    },
  });

  server.route({
    method: 'GET',
    path: '/users/:{userId}',
    handler: function (request) {
      return userService.getUser(request.params.userId);
    },
  });

  server.route({
    method: 'PATCH',
    path: '/users/:{userId}',
    handler: function (request) {
        return userService.patchUser(request.payload, request.params.userId);
    },
  });

  server.route({
    method: 'PUT',
    path: '/users',
    handler(request) {
      return userService.addUser(request.payload);
    },
  });
};
