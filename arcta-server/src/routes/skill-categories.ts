import * as skillCategoryService from '../services/skill-category-service';

export default (server) => {
  server.route({
    method: 'GET',
    path: '/skill_categories',
    handler() {
      return skillCategoryService.getSkillCategories();
    },
  });

  server.route({
    method: 'PUT',
    path: '/skill_categories',
    handler(request) {
      return skillCategoryService.addSkillCategory(request.payload);
    },
  });
};
