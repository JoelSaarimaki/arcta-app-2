import * as skillService from '../services/skill-service';

export default (server) => {
  server.route({
    method: 'GET',
    path: '/skills',
    handler() {
      return skillService.getSkills();
    },
  });

  server.route({
    method: 'PUT',
    path: '/skills',
    handler(request) {
      return skillService.addSkill(request.payload);
    },
  });
};
