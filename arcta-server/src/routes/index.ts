import users from './users';
import skills from './skills';
import skillCategories from './skill-categories';
import usersSkills from './users-skills';

export default (server) => {
  users(server);
  skills(server);
  skillCategories(server);
  usersSkills(server)
};
