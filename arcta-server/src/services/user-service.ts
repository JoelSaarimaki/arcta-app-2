import { connect } from '../database';

const knex = connect();

export async function getUsers() {
  // voit validoida tässä (): tyyppi
  return await knex
    .select(
      'users.*',
      knex.raw('json_agg(skills.*) as skills'),
    )
    .from('users')
    .leftJoin(
      'users_skills',
      'users.id',
      'users_skills.user_id',
    )
    .leftJoin(
      'skills',
      'skills.id',
      'users_skills.skill_id',
    )
    .groupBy('users.id');
}

export async function getUser(id: number) {
  // voit validoida tässä (): tyyppi
  // ANNA USER-VASTAUKSELLE TYYYPPIIIIIIII
  const [user] =await knex
    .select(
      'users.*',
      knex.raw('json_agg(skills.*) as skills'),
    )
    .where('users.id', id)
    .from('users')
    .leftJoin(
      'users_skills',
      'users.id',
      'users_skills.user_id',
    )
    .leftJoin(
      'skills',
      'skills.id',
      'users_skills.skill_id',
    )
    .groupBy('users.id');
  // (En ehkä suosittele heittää virhettä tollee ku näytin.
  // Hapissa on oma NotFound-virhesysteeminsä)
  if (!user)
  {
    throw new Error(`User with id ${id} was not found!`);
  }
  return user;
}

export async function patchUser(user, id: number) {
  // voit validoida tässä (): tyyppi
  // ANNA USER-VASTAUKSELLE TYYYPPIIIIIIII
  const [patchedUser] =await knex('users')
    .where('users.id', id)
    .from('users')
    .update(user)
    .returning('*');
  // (En ehkä suosittele heittää virhettä tollee ku näytin.
  // Hapissa on oma NotFound-virhesysteeminsä)
  if (!patchedUser)
  {
    throw new Error(`User with id ${id} was not found!`);
  }
  return patchedUser;
}

export async function addUser(user) {
  const [newUser] = await knex('users')
    .insert(user)
    .returning('*');
  return newUser;
}
