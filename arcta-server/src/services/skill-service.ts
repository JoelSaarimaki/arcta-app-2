import { connect } from '../database';

const knex = connect();

export async function getSkills() {
  return await knex('skills').select();
}

export async function addSkill(skill) {
  const [newSkill] = await knex('skills')
    .insert(skill)
    .returning('*');
  return newSkill;
}
