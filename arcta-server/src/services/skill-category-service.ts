import { connect } from '../database';

const knex = connect();

export async function getSkillCategories() {
  // voit validoida tässä (): tyyppi
  return await knex
    .select(
      'skill_categories.*',
      knex.raw('json_agg(skills.*) as skills'),
    )
    .from('skill_categories')
    .leftJoin(
      'skills',
      'skills.skill_category_id',
      'skill_categories.id',
    )
    .groupBy('skill_categories.id');
}

export async function addSkillCategory(skillCategory) {
  const [newSkillCategory] = await knex('skill_categories')
    .insert(skillCategory)
    .returning('*');
  return newSkillCategory;
}
