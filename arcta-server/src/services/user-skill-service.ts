import { connect } from '../database';
//import Logger from '../logger';

//const logger = Logger(__filename);

interface UserSkill {
  user_id: number;
  skill_id: number;
}

const knex = connect();

export async function getUserSkill() {
  // voit validoida tässä (): tyyppi
  return await knex
    .select(
      'users_skills.*',
    )
    .from('users_skills');
}

export async function addUserSkill(userSkill: UserSkill) {
  const [newUserSkill] = await knex('users_skills')
    .insert(userSkill)
    .returning('*');
  return newUserSkill;
}

export async function delUserSkill(userSkill: UserSkill) {
  const [newUserSkill] = await knex('users_skills')
    .where(userSkill)
    .del()
    .returning('*');
  return newUserSkill;
}

export async function delUserSkills(userSkill: UserSkill) {
  const [newUserSkill] = await knex('users_skills')
    .where('user_id', userSkill.user_id)
    .del()
    .returning('*');
  return newUserSkill;
}
