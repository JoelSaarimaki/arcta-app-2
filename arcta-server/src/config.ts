namespace Config {
  export const port = process.env.PORT;

  export const env = {
    test: process.env.NODE_ENV === 'test',
    dev: process.env.NODE_ENV === 'development',
    prod: process.env.NODE_ENV === 'production',
  };

  export const logging = {
    level: process.env.LOGGER_LEVEL,
  };

  export const pg = {
    url: process.env.DATABASE_URL,
  };
}

export default Config;
