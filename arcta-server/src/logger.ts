import * as path from 'path';
import { createLogger, transports, format } from 'winston';
import * as _ from 'lodash';
import config from './config';

const { combine, label, timestamp, splat, printf, colorize } = format;

enum LoggingLevelEnum {
  silly = 'silly',
  debug = 'debug',
  info = 'info',
  warn = 'warn',
  error = 'error',
}

// Is there better way to do this?
const passthrough = format(_.identity);

export default (filePath: string) => createLogger({
  level: LoggingLevelEnum[config.logging.level as any] || LoggingLevelEnum.info,
  format: combine(
    label({ label: path.basename(filePath) }),
    config.env.prod ? passthrough() : colorize(),
    timestamp(),
    splat(),
    printf((info) => {
      return `${info.timestamp} [${info.label}] ${info.level}: ${info.message} ${JSON.stringify(info.meta) || ''}`;
    }),
  ),
  transports: _.compact([
    new transports.Console(),
  ]),
});
