import * as Hapi from 'hapi';
import routes from './routes';
import Logger from './logger';
import Config from './config';

const logger = Logger(__filename);

// Create a server with a host and port
const server = Hapi.server({
  host: 'localhost',
  port: Config.port,
  routes: {
    cors: true,
  },
  debug: { request: ['error'] }
});

routes(server);

// Start the server
const start = async () => {
  try {
    await server.start();
  } catch (err) {
    logger.info(err);
    process.exit(1);
  }

  logger.info('Server running at:', server.info.uri);
};

start();
