const dbConfig = {
  client: 'pg',
  connection: process.env.DATABASE_URL,
  migrations: {
    tableName: 'migrations',
  }
};

const knexConfig = {
  development: dbConfig,
  test: dbConfig,
  production: dbConfig,
};

const env = process.env.NODE_ENV;

if (!Object.prototype.hasOwnProperty.call(knexConfig, env)) {
  console.error('Invalid NODE_ENV value', env);
}

module.exports = knexConfig;
