﻿# Jonken superhärveli

```bash
npm i

docker-compose up

knex migrate:make
knex migrate:latest
knex migrate:rollback
```

Joka kerta:
```bash
source .env
npm start
```
