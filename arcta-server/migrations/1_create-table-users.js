const { stripIndent } = require('common-tags');

exports.up = async function(knex) {
  await knex.schema.createTable('users', function (table) {
    table.increments('id');
    table.string('name');
    table.binary('profile_image');
    table.binary('header_image');
    table.integer('age');
    table.string('school'); table.string('faculty'); table.string('degree');
    table.string('phone'); table.string('mail'); table.string('address');
    table.string('city'); table.string('country'); table.string('speech');
    table.string('availability'); table.string('travel'); table.string('cv');
    table.string('portfolio');
    table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
    table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
  })
  .createTable('skill_categories', function (table) {
    table.increments('id');
    table.string('name');
    table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
    table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
  })
  .createTable('skills', function (table) {
    table.increments('id');
    table.string('name');
    table.integer('skill_category_id').unsigned().notNullable();
    table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
    table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));

    table.foreign('skill_category_id').references('skill_categories.id')
      .onDelete('CASCADE').onUpdate('CASCADE');
  })
  .createTable('users_skills', function (table) {
    table.integer('user_id').unsigned().notNullable();
    table.integer('skill_id').unsigned().notNullable();

    table.unique(['user_id', 'skill_id']);

    table.foreign('user_id').references('users.id')
      .onDelete('CASCADE').onUpdate('CASCADE');
    table.foreign('skill_id').references('skills.id')
      .onDelete('CASCADE').onUpdate('CASCADE');
  })
  .raw(stripIndent`
    CREATE TRIGGER set_timestamp
    BEFORE UPDATE ON users
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp();
  `);
};

exports.down = async function(knex) {
  await knex.schema.dropTable('users_skills')
    .dropTable('skill_categories_skills')
    .dropTable('skills')
    .dropTable('skill_categories')
    .dropTable('users');
};
