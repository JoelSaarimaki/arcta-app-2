import * as path from 'path';
import { Configuration, DefinePlugin, ContextReplacementPlugin } from 'webpack';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

const paths = {
  dist: path.resolve(__dirname, 'dist'),
  src: path.join(__dirname, 'src'),
};

const isProd = process.env.NODE_ENV === 'production';

const config: Configuration = {
  entry: path.join(paths.src, 'index.tsx'),

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.(jpg|jpeg|gif|png|svg)$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          name: 'assets/img/[hash:6].[ext]',
        },
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: 'assets/fonts/[hash:6].[ext]',
        },
      },
    ],
  },

  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    plugins: [
      new TsconfigPathsPlugin(),
    ],
  },

  output: {
    filename: `[name].[${isProd ? 'chunkhash' : 'hash'}:6].js`,
    path: paths.dist,
    publicPath: '/',
  },

  devServer: {
    host: 'localhost',
    port: 3001,
    contentBase: paths.src,
    historyApiFallback: true,
    stats: {
      warnings: false,
    },
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        react: {
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          name: 'react',
          chunks: 'all',
        },
        antd: {
          test: /[\\/]node_modules[\\/](antd|@ant-design)[\\/]/,
          name: 'antd',
          chunks: 'all',
        },
      },
    },
  },

  plugins: [
    new DefinePlugin({
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    }),

    new ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb/),

    new HtmlWebpackPlugin({
      title: 'Feedbuilder',
      template: path.join(paths.src, 'index.html'),
      meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
    }),

    // new BundleAnalyzerPlugin(),
  ],
};

export default config;
