import 'antd/dist/antd.min.css';
import '@assets/fonts/fonts.css';

export const colors = {
  blue: '#01215b',
  orange: '#ff6800',
  red: '#d0111e',
  grey: '#d8d8d8',
};
