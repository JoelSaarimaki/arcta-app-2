import * as React from 'react';
import * as _ from 'lodash';
import { Form, Input, Button, Tag } from 'antd';
import { get, convertAndPatchUser } from 'utils/requests';
import { User, Skill } from 'utils/interfaces';
import { Row, Col } from 'react-flexbox-grid';

const { TextArea } = Input;

interface Props {
  userId: number;
}

interface State {
  loaded: boolean;
  user: User;
  newSkills: Skill[];
  formBasicItems: string[][];
}

// USER SUBMITTING BUGS OUT BECAUSE USERS DON'T HAVE SKILLS IN DB

/* IMPLEMENT A METHOD OF CUTTING OUT SKILLCATEGORIES AND SKILLS
FROM USERS AND THEN UPDATING THOSE AS users_skills TABLE UPDATES
INSTEAD */

export default class UserForm extends React.Component<Props, State> {
  constructor(props :Props) {
    super(props);
      this.state = {
        loaded: false,
        user: undefined,
        newSkills: [],
        formBasicItems: [
          ["name", "Name", "required"],
          ["age", "Age", "required"],
          ["school", "School", "required"],
          ["faculty", "Faculty", "required"],
          ["degree", "Degree", "required"],
          ["phone", "Phone", ""],
          ["mail", "Address", ""],
          ["city", "City", "required"],
          ["country", "Country", "required"],
        ],
      };
    }

  async componentDidMount() {
    let userIdPath :string = "/users/:" + this.props.userId;
    const res = await get<User>({path: userIdPath});
    console.log(res);
    this.setState({ user: res });

    // Make a copy of skills index list to compare it later to the original.
    // This list is used instead of the skills list of the user to keep
    // track of skills.
    this.setState({ newSkills: res.skills})

    this.setState({ loaded: true });
  }

  createForm() {
    let formElements:any = [];
    let increment:number = 1;

    this.state.formBasicItems.forEach((item) => {
      formElements.push(
        this.createFormElement(item, increment)//, spaceIncrement)
      );
    });

    return (formElements);
  }

  createFormElement(item:string[], key:number) { //, sI:number) {
    // get all required values
    let user = this.state.user;
    let value = user[item[0]];
    let name = item[1];
    let returnItems:any = [];

      returnItems.push(
        <Col xs={24} sm={12} md={6} lg={3} key={key*100}>
          <Form.Item key={key} style={{marginBottom: "0.6em"}}>
            <Input value={value} placeholder={name}
              onChange={(e) => this.handleFormChange(item[0], e)}/>
          </Form.Item>
        </Col>
      );

    return(returnItems);
  }

  createBadges() {
    let increment: number = 1;
    const badges: any = [];
    this.state.newSkills.forEach((newSkill) => {
      badges.push(
        <Tag key={increment}
          onClick={() => this.skillRemove(newSkill)}>
          {newSkill.name}
        </Tag>
      );
      increment++;
    });
    return (
      badges
    );
  }

  skillRemove(skill: Skill) {
    let newSkillsEdited: Skill[] = this.state.newSkills;
    _.pull(newSkillsEdited, skill);
    this.setState({newSkills: newSkillsEdited});
    console.log("skill removed: " + skill.name);
  }

  skillAdd(skill: Skill) {
    let newSkillsEdited: Skill[] = this.state.newSkills;
    newSkillsEdited.push(skill);
    this.setState({newSkills: newSkillsEdited});
    console.log("skill added: " + skill);
  }

  handleFormChange(source:string, event:any) {
    // modifications -> new variable -> state.user
    let userEdited: User = this.state.user;
    userEdited[source] = event.target.value
    this.setState({user: userEdited});
  }

  async submit() {
    convertAndPatchUser(
      this.state.user,
      this.state.newSkills
    );
  }

  render() {
    //if (this.props.userId != undefined) this.getUser();
    return (
      <div>

        <Row>
          { this.state.user != undefined ? this.createForm() : "" }
        </Row>

        <TextArea rows={2} style={{marginBottom: "1em", marginTop: "0.3em"}}/>

        <div style={{display: "flex", marginBottom: "1em"}}>
          { this.state.loaded ? this.createBadges() : "" }
        </div>

        <Button
          type="primary"
          onClick={(e) => this.submit()}
        >
          Submit
        </Button>

      </div>
    );
  }
}
