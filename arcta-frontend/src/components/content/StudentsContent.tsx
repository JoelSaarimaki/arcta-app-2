import * as React from 'react';
import * as _ from 'lodash';
import { get } from 'utils/requests';

interface Props {
  userId: number;
}

interface State {
  loaded: boolean;
  students: Student[];
}

interface Student {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
}

// Create the class to hold the Dropdown buttons
export default class SectionStudents extends React.Component<Props, State> {
  constructor(props :Props) {
    super(props);
      this.state = {
      loaded: false,
      students: [],
      };
    }

  async componentDidMount() {
    const res = await get<Student[]>({path: "/users"});
    console.log(res);
    this.setState({ students: res });
  }

  createCards() {
    const studentCard: any = [];
    this.state.students.forEach((student) => {
        studentCard.push(this.createCard(student));
    });
    return (studentCard);
  }

  createCard(student: Student) {
    return (
      <div style={{
        background: '#fff',
        width: '100%',
        minHeight: '10em',
        marginBottom: '1em'}}>
        <div style={{ padding: '1em'}}>
          {student.name}
        </div>
      </div>
    );
  }

  createSkillBadges(student: Student) {
    return (
      student
    );
  }

  render() {
    return (
      <div>
        {
          this.createCards()
        }
      </div>
    );
  }
}
