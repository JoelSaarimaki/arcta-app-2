import * as React from 'react';
import * as _ from 'lodash';
import { get } from 'utils/requests';
import { Menu, Icon, Button, Dropdown, Tag } from 'antd';
import { User, Skill, SkillCategory } from 'utils/interfaces';
import { Provider, Subscribe, Container } from 'unstated-typescript';

interface Props {
  //masterElement: any;
  skills: number[];
  collapsed: boolean;
}

interface State {
  loaded: boolean;
  skillCategories: SkillCategory[];
  selectedSkillCategories: SkillCategory[];
  selectedSkills: Skill[];
  users: User[];
}

export default class SkillManager extends React.Component<Props, State> {
  constructor(props :Props) {
    super(props);
    this.state = {
      loaded: false,
      skillCategories: [],
      selectedSkillCategories: [],
      selectedSkills: [],
      users: [],
    };
  }

  async componentDidMount() {
    const res = await get<SkillCategory[]>({path: "/skill_categories"});
    this.setState({ skillCategories: res });
    const res2 = await get<User[]>({path: "/users"});
    // add skill categories to students
    res2.forEach((user) => {
      // console.log("User Logged: " + user.name);
      // console.log(user);
      user.skillCategories = [] as SkillCategory[];
      // if no skills have been assinged, at least give table
      if (user.skills[0] == null) user.skills = [] as Skill[];
      let skillCategoryIds: number[] = [];
        // ALERT, DEPENDS ON DATABASE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        skillCategoryIds = _.uniq(_.map(user.skills, 'skill_category_id'));
        skillCategoryIds.forEach(categoryId => {
          if (categoryId != undefined) {
          let skillCategoryIndex: number =
            _.findIndex(this.state.skillCategories, {id: categoryId});
          user.skillCategories.push(this.state.skillCategories[skillCategoryIndex]);
          }
        });
    });
    this.setState({ users: res2 });
    this.setState({ loaded: true });
  }

  // CATEGORY MANAGEMENT ===========================================

  categoryAdd(category: SkillCategory) {
    if (!_.includes(this.state.selectedSkillCategories, category)){
      let newObject = this.state.selectedSkillCategories
      newObject.push(category);
      this.setState({ selectedSkillCategories: newObject });
    }
    // console.log("added Category: " + category.name);
  }

  skillCategoryRemove(category: SkillCategory) {
    if (_.includes(this.state.selectedSkillCategories, category)){
      let newObject = this.state.selectedSkillCategories
      _.pull(newObject, category);
      this.setState({ selectedSkillCategories: newObject });
      // remove all skills of category
      let newSkills = this.state.selectedSkills;
      category.skills.forEach((skill) => {
        _.pull(newSkills, skill);
      });
      this.setState({ selectedSkills: newSkills });
    }
    // console.log("removed: " + category.name);
  }

  // SKILL MANAGEMENT ==============================================

  skillAdd(masterElement: any, skill: Skill) {
    if (!_.includes(this.state.selectedSkills, skill)){
      let newObject = this.state.selectedSkills
      newObject.push(skill);
      this.setState({ selectedSkills: newObject });
    }
    masterElement.updateSelectedSkills(this.state.selectedSkills);
    // console.log("added Skill: " + skill.name);
  }

  skillRemove(masterElement: any, skill: Skill) {
    if (_.includes(this.state.selectedSkills, skill)){
      let newObject = this.state.selectedSkills
      _.pull(newObject, skill);
      this.setState({ selectedSkills: newObject });
    }
    masterElement.updateSelectedSkills(this.state.selectedSkills);
    // console.log("removed: " + skill.name);
  }


  createSkillCategoriesOverlay() {
    const menuItems: any = [];
    let increment: number = 1;
    this.state.skillCategories.forEach((skillCategory) => {
      // we do not need categories that do not have skills to them
      if (!skillCategory.skills.includes(null)) {
        menuItems.push(
          <Menu.Item key={increment}
            onClick={() => this.categoryAdd(skillCategory)}
          >
            {skillCategory.name}
          </Menu.Item>
        );
        increment++;
      }
    });
    return (
      <Menu>
        {menuItems}
      </Menu>
    );
  }

  createSkillsOverlay(masterElement: any) {
    const menuItems: any = [];
    let increment: number = 1;
    this.state.selectedSkillCategories.forEach((skillCategory) => {
      console.log(skillCategory);
      if (skillCategory.skills !== undefined) {
        skillCategory.skills.forEach((skill) => {
          menuItems.push(
            <Menu.Item key={increment}
              onClick={() => this.skillAdd(masterElement, skill)}
            >
              {skill.name}
            </Menu.Item>
          );
          increment++;
        });
      }
    });
    return (
      <Menu>
        {menuItems}
      </Menu>
    );
  }

  createBadges(masterElement: any) {
    let increment: number = 1;
    const badges: any = [];
    this.state.selectedSkillCategories.forEach((skillCategory) => {
      if (skillCategory.name != undefined) {
        badges.push(
          <Tag key={increment}
            onClick={() => this.skillCategoryRemove(skillCategory)} color="#108ee9">
            {skillCategory.name}
          </Tag>
        );
        increment++;
      }
    });
    increment = 1;
    this.state.selectedSkills.forEach((skill) => {
      if (skill.name != undefined) {
        badges.push(
          <Tag key={increment}
            onClick={() => this.skillRemove(masterElement, skill)}>
            {skill.name}
          </Tag>
        );
        increment++;
      }
    });
    return (
      badges
    );
  }

  render() {
    return (/*
      <div style={{ minHeight: '50%', overflow: 'hidden'}}>

      <Subscribe to={[this.props.masterElement]}>
        {masterElement => (
          <div>
          <div className="buttons" style={{marginBottom: '1em' }}>
          {this.props.collapsed ? "" :(
            <Dropdown overlay={this.createSkillCategoriesOverlay()}>
              <Button style={{margin: '1em 0 0em 1em' }} type="primary">
                Actions <Icon type="down" />
              </Button>
            </Dropdown>
          )}
          {this.props.collapsed ? "" :(
            <Dropdown overlay={this.createSkillsOverlay(masterElement)}>
              <Button style={{margin: '1em 0 0em 1em' }}>
                Actions <Icon type="down" />
              </Button>
            </Dropdown>
          )}
        </div>

        { this.state.loaded ? this.createBadges(masterElement) : "" }
        </div>
        )}
      </Subscribe>
      </div>
    */<div></div>);
  }
}
