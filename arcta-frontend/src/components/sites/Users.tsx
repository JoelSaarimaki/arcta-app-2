import * as React from 'react';
import * as _ from 'lodash';
import { get } from 'utils/requests';
import { Layout, Menu, Icon, Button, Dropdown, Tag } from 'antd';
import { User, Skill, SkillCategory } from 'utils/interfaces';
import { Provider, Subscribe, Container } from 'unstated-typescript';

import StudentsContent from '../content/StudentsContent';
import SkillManager from '../content/SkillManager';
import PageHeader from '../PageHeader';

const { Header, Sider, Content } = Layout;

interface Props {
  userId: number;
}

interface State {
  loaded: boolean;
  collapsed: boolean;
  skillCategories: SkillCategory[];
  selectedSkillCategories: SkillCategory[];
  selectedSkills: Skill[];
  users: User[];
}

// CLASS ===========================================================

export default class Users extends React.Component<Props, State> {
  constructor(props :Props) {
    super(props);
    this.state = {
      loaded: false,
      collapsed: false,
      skillCategories: [],
      selectedSkillCategories: [],
      selectedSkills: [],
      users: [],
    };
  }

  async componentDidMount() {
    const res = await get<SkillCategory[]>({path: "/skill_categories"});
    this.setState({ skillCategories: res });
    const res2 = await get<User[]>({path: "/users"});
    // add skill categories to students
    res2.forEach((user) => {
      // console.log("User Logged: " + user.name);
      // console.log(user);
      user.skillCategories = [] as SkillCategory[];
      // if no skills have been assinged, at least give table
      if (user.skills[0] == null) user.skills = [] as Skill[];
      let skillCategoryIds: number[] = [];
        // ALERT, DEPENDS ON DATABASE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        skillCategoryIds = _.uniq(_.map(user.skills, 'skill_category_id'));
        skillCategoryIds.forEach(categoryId => {
          if (categoryId != undefined) {
          let skillCategoryIndex: number =
            _.findIndex(this.state.skillCategories, {id: categoryId});
          user.skillCategories.push(this.state.skillCategories[skillCategoryIndex]);
          }
        });
    });
    this.setState({ users: res2 });
    this.setState({ loaded: true });
  }

  // CATEGORY MANAGEMENT ===========================================

  categoryAdd(category: SkillCategory) {
    if (!_.includes(this.state.selectedSkillCategories, category)){
      let newObject = this.state.selectedSkillCategories
      newObject.push(category);
      this.setState({ selectedSkillCategories: newObject });
    }
    // console.log("added Category: " + category.name);
  }

  skillCategoryRemove(category: SkillCategory) {
    if (_.includes(this.state.selectedSkillCategories, category)){
      let newObject = this.state.selectedSkillCategories
      _.pull(newObject, category);
      this.setState({ selectedSkillCategories: newObject });
      // remove all skills of category
      let newSkills = this.state.selectedSkills;
      category.skills.forEach((skill) => {
        _.pull(newSkills, skill);
      });
      this.setState({ selectedSkills: newSkills });
    }
    // console.log("removed: " + category.name);
  }

  // SKILL MANAGEMENT ==============================================

  skillAdd(skill: Skill) {
    if (!_.includes(this.state.selectedSkills, skill)){
      let newObject = this.state.selectedSkills
      newObject.push(skill);
      this.setState({ selectedSkills: newObject });
    }
    // console.log("added Skill: " + skill.name);
  }

  skillRemove(skill: Skill) {
    if (_.includes(this.state.selectedSkills, skill)){
      let newObject = this.state.selectedSkills
      _.pull(newObject, skill);
      this.setState({ selectedSkills: newObject });
    }
    // console.log("removed: " + skill.name);
  }


  createSkillCategoriesOverlay() {
    const menuItems: any = [];
    let increment: number = 1;
    this.state.skillCategories.forEach((skillCategory) => {
      // we do not need categories that do not have skills to them
      if (!skillCategory.skills.includes(null)) {
        menuItems.push(
          <Menu.Item key={increment}
            onClick={() => this.categoryAdd(skillCategory)}
          >
            {skillCategory.name}
          </Menu.Item>
        );
        increment++;
      }
    });
    return (
      <Menu>
        {menuItems}
      </Menu>
    );
  }

  createSkillsOverlay() {
    const menuItems: any = [];
    let increment: number = 1;
    this.state.selectedSkillCategories.forEach((skillCategory) => {
      console.log(skillCategory);
      if (skillCategory.skills !== undefined) {
        skillCategory.skills.forEach((skill) => {
          menuItems.push(
            <Menu.Item key={increment}
              onClick={() => this.skillAdd(skill)}
            >
              {skill.name}
            </Menu.Item>
          );
          increment++;
        });
      }
    });
    return (
      <Menu>
        {menuItems}
      </Menu>
    );
  }

  createBadges() {
    let increment: number = 1;
    const badges: any = [];
    this.state.selectedSkillCategories.forEach((skillCategory) => {
      if (skillCategory.name != undefined) {
        badges.push(
          <Tag key={increment}
            onClick={() => this.skillCategoryRemove(skillCategory)} color="#108ee9">
            {skillCategory.name}
          </Tag>
        );
        increment++;
      }
    });
    increment = 1;
    this.state.selectedSkills.forEach((skill) => {
      if (skill.name != undefined) {
        badges.push(
          <Tag key={increment}
            onClick={() => this.skillRemove(skill)}>
            {skill.name}
          </Tag>
        );
        increment++;
      }
    });
    return (
      badges
    );
  }

  filterUsers() {
    let users = this.state.users;
    let filteredUsers: User[] = [];
    if (this.state.selectedSkillCategories.length > 0) {
      users.forEach((user) => {
        let sSC = this.state.selectedSkillCategories;
        let sS = this.state.selectedSkills;
        if (_.intersectionWith(sSC, user.skillCategories, _.isEqual).length === sSC.length &&
        _.intersectionWith(sS, user.skills, _.isEqual).length === sS.length) {
          filteredUsers.push(user);
        }
      });
      return (filteredUsers);
    } else return (users);
  }

  makeUserCards() {
    let increment: number = 1;
    let users = this.filterUsers();
    let userCards: any = [];
    users.forEach((user) => {
      let skillCategories: any = [];
      user.skillCategories.forEach(category => {
        if (category != null) {
          skillCategories.push(
            category.name
          );
        }
      });
      let skills: any = [];
      user.skills.forEach(skill => {
        if (skill != null) {
          skills.push(
            skill.name
          );
        }
      });
      userCards.push(
        <div key={increment} style={{
          background: '#fff',
          width: '100%',
          minHeight: '10em',
          marginBottom: '1em'}}>
          <div style={{ padding: '1em'}}>
            {user.name}
            {" " + skillCategories}
            {" " + skills}
          </div>
        </div>
      );
      increment++;
    })
    return (userCards);
  }

  updateSelectedSkills(newSelectedSkills: Skill[]) {
    this.setState({ selectedSkills: newSelectedSkills });
  }

  render() {
    return (
      <Layout>

        <PageHeader selectedKey="1" component={this} />

        <Layout>
          <Sider
            width={250}
            style={{ background: '#fff', height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}

            trigger={null}
            collapsible
            collapsed={this.state.collapsed}
          >
            {/*
            <Provider>
              <SkillManager
                masterElement={this}
                skills={[]}
                collapsed={this.state.collapsed}/>
            </Provider>
            */}
            {
            <div style={{ minHeight: '50%', overflow: 'hidden'}}>
              <div className="buttons" style={{marginBottom: '1em' }}>
                {this.state.collapsed ? "" :(
                  <Dropdown overlay={this.createSkillCategoriesOverlay()}>
                    <Button style={{margin: '1em 0 0em 1em' }} type="primary">
                      Actions <Icon type="down" />
                    </Button>
                  </Dropdown>
                )}
                {this.state.collapsed ? "" :(
                  <Dropdown overlay={this.createSkillsOverlay()}>
                    <Button style={{margin: '1em 0 0em 1em' }}>
                      Actions <Icon type="down" />
                    </Button>
                  </Dropdown>
                )}
              </div>
              { this.state.loaded ? this.createBadges() : "" }

            </div>
            }
            {this.state.collapsed ? "" :(
              <div>
                <Menu
                  mode="inline"
                  defaultSelectedKeys={['1']}
                  defaultOpenKeys={['sub1']}
                  style={{ height: '100%', borderRight: 0 }}
                >
                  <Menu.Item key="1">
                    <Icon type="desktop" />
                    <span>Option 1</span>
                  </Menu.Item>
                </Menu>
              </div>
            )}
          </Sider>
          <Layout style={{ padding: '24px 24px 24px' }}>
            <Content style={{
              padding: 0, margin: 0, minHeight: 280,
            }}
            >
              { this.state.loaded ? this.makeUserCards() : "" }
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}



/*  WAY FOR GIVING USERS SKILL CATEGORIES

      user.skills.forEach((skillOfUser) => {
        if (skillOfUser != null) {

        /*
        if (skillOfUser != null) {
          // test if category of skill has already been given to user
          if (!_.includes(skillCategoryIds, skillOfUser.skillCategoryId)) {
            skillCategoryIds.push(skillOfUser.skillCategoryId);
            // get index of skill category based on id
            let skillCategoryIndex: number =
              _.findIndex(this.state.skillCategories, {id: skillOfUser.skillCategoryId});
            user.skillCategories.push(this.state.skillCategories[skillCategoryIndex]);
          }
        }
      });
      */
