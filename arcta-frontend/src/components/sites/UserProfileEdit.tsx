import * as React from 'react';
import * as _ from 'lodash';
import { get } from 'utils/requests';
import { Layout, Menu, Icon, Button, Card } from 'antd';
import { User } from 'utils/interfaces';

import PageHeader from '../PageHeader';
import UserForm from '../content/UserForm';

const { SubMenu } = Menu;
const { Header, Sider, Content } = Layout;

interface Props {
  userId: number;
}

interface State {
  collapsed: boolean;
  // user: User; NOT NEEDED
}

export default class UserProfileEdit extends React.Component<Props, State> {
  constructor(props :Props) {
    super(props);
      this.state = {
      collapsed: false,
      // user: undefined, NOT NEEDED
    };
  }

  /* NOT NEEDED
  async componentDidMount() {
    let userIdPath :string = "/users/:" + this.props.userId;
    const res = await get<User>({path: userIdPath});
    this.setState({ user: res });
  }
  */

  render() {
    return (
      <Layout>

        <PageHeader selectedKey="2" component={this} />

        <Layout>
          <Sider
          width={250}
          style={{ background: '#fff', height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          >
            <div className="buttons" style={{marginBottom: '1em' }}>
              {this.state.collapsed
              ? <Button type="primary" icon="pie-chart" shape="circle" style={{margin: '1em 0 0em 1.5em' }}/> : "" }
              {this.state.collapsed
              ? <Button type="primary" icon="desktop" shape="circle" style={{margin: '1em 0 0em 1.5em' }}/> : "" }
              {this.state.collapsed
              ? <Button type="primary" icon="download" shape="circle" style={{margin: '1em 0 0em 1.5em' }}/> : "" }

              {!this.state.collapsed
              ? <Button type="primary" icon="pie-chart" style={{margin: '1em 0 0em 1em' }}>Lol</Button> : "" }
              {!this.state.collapsed
              ? <Button type="primary" icon="download" style={{margin: '1em 0 0em 1em' }}>I know you can</Button> : "" }
              {!this.state.collapsed
              ? <Button type="primary" icon="desktop" style={{margin: '1em 0 0em 1em' }}>Yolo it</Button> : "" }

              <Menu
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub0']}
                mode="inline"
                inlineCollapsed={this.state.collapsed}
              >
                <SubMenu key="sub1" title={<span><Icon type="mail" /><span>Tips</span></span>}>
                  <SubMenu key="sub2" title={<span><Icon type="mail" /><span>Navigation One</span></span>}>
                    <Card style={{ width: 300 }}>
                      <p>Card content</p>
                    </Card>
                  </SubMenu>
                  <SubMenu key="sub3" title={<span><Icon type="appstore" /><span>Navigation Two</span></span>}>
                    <SubMenu key="sub4" title="Submenu1">
                      <Card style={{ width: 300 }}>
                        <p>Card content</p>
                      </Card>
                    </SubMenu>
                    <SubMenu key="sub5" title="Submenu2">
                      <Card style={{ width: 300 }}>
                        <p>Card content</p>
                      </Card>
                    </SubMenu>
                  </SubMenu>
                </SubMenu>
              </Menu>
            </div>
          </Sider>
          <Layout style={{ padding: '24px 24px 24px' }}>
            <Content style={{
              background: '#fff', padding: 24, margin: 0, minHeight: 280,
            }}
            >

              {this.props.userId != undefined ? (
                <UserForm userId={this.props.userId}/>
                ) : ""
              }
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

/*
                <Menu.Item key="1">
                  <Icon type="pie-chart" />
                  <span>Option 1</span>
                </Menu.Item>
                <Menu.Item key="2">
                  <Icon type="desktop" />
                  <span>Option 2</span>
                </Menu.Item>
*/
