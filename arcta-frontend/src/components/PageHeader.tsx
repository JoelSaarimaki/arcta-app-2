import * as React from 'react';
import * as _ from 'lodash';
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { RadioChangeEventTarget } from 'antd/lib/radio';

const { Header } = Layout;

interface Props {
  selectedKey: string;
  component: any;
}

interface State {
  collapsed: boolean;
}

export default class PageHeader extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }

  componentDidMount() {
    const windowsWidth = this.getWidth();
    if (windowsWidth < 800) {
      this.setState({
        collapsed: true,
      });
      // set state of child
      this.props.component.setState({
        collapsed: true,
      });
    }
  }

  getWidth() {
    return Math.max(
      document.body.scrollWidth,
      document.documentElement.scrollWidth,
      document.body.offsetWidth,
      document.documentElement.offsetWidth,
      document.documentElement.clientWidth
    );
  }

  toggle = () => { // this.toggle
    // set own state
    this.setState({
      collapsed: !this.state.collapsed,
    });
    // set state of child
    this.props.component.setState({
      collapsed: !this.state.collapsed,
    });
  }


  render() {
    return (
      <Header className="header"
        style={{ height: '3em', padding: 0 }}>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={[this.props.selectedKey]}
          style={{ lineHeight: '3em' }}
        >
        <Icon
          style={{ padding: '1em' }}
          className="trigger"
          type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={this.toggle}
        />
          <Menu.Item key="1"><Link to="/">home</Link></Menu.Item>
          <Menu.Item key="2"><Link to="/user-profile-edit">page 2</Link></Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </Header>
    );
  }
}


