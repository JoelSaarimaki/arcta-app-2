namespace Config {
  export const env = {
    prod: NODE_ENV === 'production',
    dev: NODE_ENV === 'development',
  };

  export const backend = env.prod ? '' : 'http://localhost:5000';
}

export default Config;
