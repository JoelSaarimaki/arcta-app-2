import * as React from 'react';
import * as _ from 'lodash';
import './App.css';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';

import PageHeader from './components/PageHeader';
import Users from './components/sites/Users';
import UserProfileEdit from './components/sites/UserProfileEdit';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

export default class App extends React.Component {
  state = {
    userId: undefined as number,
    username: undefined as string,
    password: undefined as string,
    loaded: false,
  };

  // PageUsers = () => <UsersGrommet userId={this.state.userId}/>;
  PageUsers = () => <Users userId={this.state.userId}/>;
  PageUserProfileEdit = () => <UserProfileEdit userId={this.state.userId}/>;

  render() {
    // placeholder user load
    if (!this.state.loaded) {
      this.setState({ userId: 1 });
      this.setState({ loaded: true });
    }
    return (
      <BrowserRouter>
        <Switch>
          <div className="App" style={{ fontFamily: 'SkattaSans' }}>
            <Route path="/" exact component={this.PageUsers} />
            <Route path="/user-profile-edit" component={this.PageUserProfileEdit} />
          </div>
        </Switch>
      </BrowserRouter>
    );
  }
}

/*

export default (
    props: {

    }
  ) => {
  return (
    <BrowserRouter>
      <Switch>
        <div className="App" style={{ fontFamily: 'SkattaSans' }}>
          <Route path="/" exact component={PageUsers} />
          <Route path="/user-profile-edit" component={PageUserProfileEdit} />
        </div>
      </Switch>
    </BrowserRouter>
  );
};
/*

toggle = () => { // this.toggle
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

export default (props: {}) => {
  return (
    <BrowserRouter>

      <Header className="header"
        style={{ height: '3em', padding: 0 }}>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{ lineHeight: '3em' }}
        >
        <Icon
          style={{ padding: '1em' }}
          className="trigger"
          type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={this.toggle}
        />
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </Header>

      <Layout>
          <Sider
          width={300}
          style={{ background: '#fff', height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          >
            <Switch>
              <div className="App" style={{ fontFamily: 'SkattaSans' }}>
                <Route path="/" exact component={PageUsers} />
                <Route path="/user-profile-edit" component={PageUserProfileEdit} />
              </div>
            </Switch>
          </Sider>
      </Layout>

      <Switch>
        <div className="App" style={{ fontFamily: 'SkattaSans' }}>
          <Route path="/" exact component={PageUsers} />
          <Route path="/user-profile-edit" component={PageUserProfileEdit} />
        </div>
      </Switch>

    </BrowserRouter>
  );
};
*/
