import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as moment from 'moment';
import './styles';
import App from './App';

moment.locale('fi');

const render = () => {
  ReactDOM.render(
    <App />,
    document.getElementById('root'),
  );
};

module.hot && module.hot.accept();

render();
