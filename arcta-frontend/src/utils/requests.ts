import * as _ from 'lodash';
import Axios, { AxiosResponse } from 'axios';
import Config from '../config';
// UserForServer WOULD BE CLAIMED TO BE MISSING BUT THAT IS NOT TRUE
import { User, Skill } from 'utils/interfaces';

interface Options {
  path: string;
}

export async function get<T>(options: Options) {
  const path: string = Config.backend + options.path;
  console.log(Config.backend);
  const res: AxiosResponse<T> = await Axios.get(path);
  return res.data;
}

export async function patch<T>(options: Options, object: any) {
  const path: string = Config.backend + options.path;
  console.log(Config.backend);
  const res: AxiosResponse<T> = await Axios.patch(path, object);
  return res.data;
}

/*
interface Options2 {
  path: string;
  index: string;
}

export async function getWithIndex<T>(options: Options2) {
  const path: string = Config.backend + options.path + options.index;
  console.log(Config.backend);
  const res: AxiosResponse<T> = await Axios.get(path);
  return res.data;
}
*/


// HANDLERS

interface UserForServer {
  name: string;
  profile_image: any;
  header_image: any;
  age: number;
  school: string;
  faculty: string;
  degree: string;
  phone: string;
  mail: string;
  address: string;
  city: string;
  country: string;
  speech: string;
  availability: string;
  travel: string;
  cv: string;
  portfolio: string;

  [key: string]: any;
}

interface UserSkillForServer {
  user_id: number;
  skill_id: number;
}

export async function convertAndPatchUser(user: User, newSkillList: Skill[]) {
  // prepare user for being sent
  let userId = user.id;
  let userAge = +user.age; // conversion from string to number
  let userForServer: UserForServer = {
    name: user.name,
    profile_image: user.profile_image,
    header_image: user.header_image,
    age: userAge,
    school: user.school,
    faculty: user.faculty,
    degree: user.degree,
    phone: user.phone,
    mail: user.mail,
    address: user.address,
    city: user.city,
    country: user.country,
    speech: user.speech,
    availability: user.availability,
    travel: user.travel,
    cv: user.cv,
    portfolio: user.portfolio,
  };
  JSON.stringify(user);

  // prepare skills for being sent
  let newSkills: UserSkillForServer[] = [];
  let deletedSkills: UserSkillForServer[] = [];
  newSkillList.forEach((skill) => {
    if (!_.includes(user.skills, skill)) {
      newSkills.push({
        user_id: user.id,
        skill_id: skill.id
      });
    }
  });
  user.skills.forEach((skill) => {
    if (!_.includes(newSkillList, skill)) {
      deletedSkills.push({
        user_id: user.id,
        skill_id: skill.id
      });
    }
  });


  // send user json
  let userIdPath: string = "/users/:" + userId;
  const res = await patch<User>({path: userIdPath}, userForServer);

  // send skill json
  /*
  newSkills.forEach((skill) => {
    JSON.stringify(skill);
  });
  let addSkillPath: string = "/users/:" + userId;
  const res = await patch<User>({path: userIdPath}, userForServer);
  let removeSkillPath: string = "/users/:" + userId;
  const res = await patch<User>({path: userIdPath}, userForServer);
  */
}


