export interface SkillCategory {
  id: number;
  name: string;
  skills: Skill[];
}

export interface Skill {
  id: number;
  name: string;
  skillCategoryId: number;
}

export interface User {
  id: number;
  name: string;
  profile_image: any;
  header_image: any;
  age: number;
  school: string;
  faculty: string;
  degree: string;
  phone: string;
  mail: string;
  address: string;
  city: string;
  country: string;
  speech: string;
  availability: string;
  travel: string;
  cv: string;
  portfolio: string;
  skillCategories: SkillCategory[];
  skills: Skill[];

  [key: string]: any;
}

// CLAIMED TO BE MISSING
/*
export interface UserForServer {
  name: string;
  profile_image: any;
  header_image: any;
  age: number;
  school: string;
  faculty: string;
  degree: string;
  phone: string;
  mail: string;
  address: string;
  city: string;
  country: string;
  speech: string;
  availability: string;
  travel: string;
  cv: string;
  portfolio: string;

  [key: string]: any;
}
*/

/*
interface User {
  id: number;
  name: string;
  age: string;
  createdAt: string;
  updatedAt: string;
  skillCategories: SkillCategory[];
  skills: Skill[];

  [key: string]: any;
}

skills: Array<{
    id: number;
    name: string;
    skillCategoryId: number;
  }>;
*/
